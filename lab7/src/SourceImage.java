import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SourceImage extends JPanel {
    String imageName;
    Dimension d;
    public SourceImage(String filename, Dimension dim) {
        imageName = filename;
        d = dim;
    }
    public void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        BufferedImage inputPicture = null;
        try {
            super.paintComponent(g);
            inputPicture = ImageIO.read(new File(imageName));
        } catch (IOException ex) {
            System.out.println("Что-то пошло не так((");
        }
        g.drawImage(inputPicture, 0, 0,d.height, d.width, this);

    }
}
