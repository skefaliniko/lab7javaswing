import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;

import javax.imageio.*;
import java.io.*;
import java.net.*;

import javax.swing.*;

public class ImageEditor extends JPanel implements WindowListener {
    double image[][][];
    String filename;
    public String getSourceImage() {
        return filename;
    }

    public ImageEditor(Dimension d) {

        filename = JOptionPane.showInputDialog("Enter filename:",
                                                              "/home/vlad/Downloads/s.jpg");
        this.image = this.readImageFile(filename);
        this.setSize(d);
    }

    public void paintComponent(Graphics g) {
        int imageCoord[] = {0, 0};
        double tmp1, tmp2, tmp3;
        for (int x = 1; x < image.length - 1; x++)
            for (int y = 1; y < image[0].length - 1; y++) {
                tmp1 = image[x - 1][y - 1][0] * 2 + image[x - 1][y][0] * 1 + image[x - 1][y + 1][0] * 2;
                tmp2 = image[x][y - 1][0] * 1 + image[x][y][0] * 4 + image[x][y + 1][0] * 1;
                tmp3 = image[x + 1][y - 1][0] * 2 + image[x + 1][y][0] * 1 + image[x + 1][y + 1][0] * 2;
                image[x][y][0] = (tmp1 + tmp2 + tmp3) / 10;

                tmp1 = image[x - 1][y - 1][1] * 2 + image[x - 1][y][1] * 1 + image[x - 1][y + 1][1] * 2;
                tmp2 = image[x][y - 1][1] * 1 + image[x][y][1] * 4 + image[x][y + 1][1] * 1;
                tmp3 = image[x + 1][y - 1][1] * 2 + image[x + 1][y][1] * 1 + image[x + 1][y + 1][1] * 2;
                image[x][y][1] = (tmp1 + tmp2 + tmp3) / 10;

                tmp1 = image[x - 1][y - 1][2] * 2 + image[x - 1][y][2] * 1 + image[x - 1][y + 1][2] * 2;
                tmp2 = image[x][y - 1][2] * 1 + image[x][y][2] * 4 + image[x][y + 1][2] * 1;
                tmp3 = image[x + 1][y - 1][2] * 2 + image[x + 1][y][2] * 1 + image[x + 1][y + 1][2] * 2;
                image[x][y][2] = (tmp1 + tmp2 + tmp3) / 10;
            }
        g.drawImage(createImage(image), imageCoord[0], imageCoord[1], 500, 300, null);
    }

    public void windowActivated(WindowEvent e) {}
    public void windowDeactivated(WindowEvent e) {}
    public void windowOpened(WindowEvent e) {}
    public void windowClosed(WindowEvent e) {}
    public void windowClosing(WindowEvent e) {System.exit(0);}
    public void windowIconified(WindowEvent e) {}
    public void windowDeiconified(WindowEvent e) {}

    public double[][][] readImageFile(String filename)
    {
        BufferedImage image = null;
        System.out.println("Please wait...loading image...");
        System.out.flush();
        try
        {
            image = ImageIO.read(new File(filename));
        }
        catch(Exception e)
        {
            try
            {
                image = ImageIO.read(new URL(filename));
            }
            catch(Exception ex)
            {
                // Return an image with one black pixel if error.
                System.out.println("Couldn't read file " + filename );
                double ret[][][]= new double[1][1][3];
                ret[0][0][0]=0;
                ret[0][0][1]=0;
                ret[0][0][2]=0;
                return ret;
            }
        }

        System.out.println("Finished reading a " + image.getWidth() + "x" + image.getHeight());

        double ret[][][] = new double[image.getWidth()][image.getHeight()][3];
        int oneD[] = image.getRGB(0,0,image.getWidth(),image.getHeight(),null,0,image.getWidth());
        for(int i=0; i<image.getHeight(); i++)
            for(int j=0; j<image.getWidth(); j++)
                for(int k=2;k>=0; k--)
                {
                    ret[j][i][k] = (oneD[i*image.getWidth()+j] & 0x000000FF)/255.0;
                    oneD[i*image.getWidth()+j] = oneD[i*image.getWidth()+j] >> 8;
                }

        return ret;
    }

    public BufferedImage createImage(double image[][][])
    {
        int imageInt[][][] = new int[image.length][][];
        for(int i=0; i<image.length; i++)
        {
            imageInt[i]=new int[image[i].length][];
            for(int j=0; j<image[i].length; j++)
            {
                imageInt[i][j]=new int[3];
                for(int k=0; k<3; k++)
                    imageInt[i][j][k]=(int)Math.round(image[i][j][k]*255);
            }
        }

        return createImage(imageInt);
    }

    public BufferedImage createImage(int image[][][])
    {
        int width = image.length;
        int height = image[0].length;

        int oneD[] = new int[width*height];

        for(int i=0; i<width; i++)
        {
            if(image[i].length != height)
            {
                System.out.println("Image is not rectangular");
                return null;
            }

            for(int j=0; j<height; j++)
            {
                if(image[i][j].length!=3)
                {
                    System.out.println("Image does not have 3 colors");
                    return null;
                }

                for(int k=0; k<3; k++)
                    if(image[i][j][k] > 255)
                        image[i][j][k] = 255;
                    else if(image[i][j][k] < 0)
                        image[i][j][k] = 0;


                oneD[j*width+i] = 0xFF000000 |
                        (0x000000FF & image[i][j][0]) << 16 |
                        (0x000000FF & image[i][j][1]) << 8 |
                        (0x000000FF & image[i][j][2]);
            }
        }


        BufferedImage img = new BufferedImage(width,height,BufferedImage.TYPE_INT_ARGB);
        img.setRGB(0,0,width,height,oneD,0,width);
        return img;
    }
}
